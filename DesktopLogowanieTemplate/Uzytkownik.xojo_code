#tag Class
Protected Class Uzytkownik
	#tag Method, Flags = &h0
		Sub Constructor(mailU as String, hashHaslo as String)
		  Me.mailU = mailU
		  me.hashHasloU = hashHaslo
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getHashHaslo() As String
		  Return hashHasloU
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getMail() As String
		  Return mailU
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getPoziomUprawnien() As Integer
		  Return poziomUprawnien
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub setPoziomUprawnien(poziomUprawnien as Integer)
		  me.poziomUprawnien = poziomUprawnien
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected hashHasloU As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected mailU As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected poziomUprawnien As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

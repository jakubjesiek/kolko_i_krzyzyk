#tag Class
Protected Class Demo
	#tag Method, Flags = &h0
		Sub Constructor(id as Integer, pole1 as String, pole2 as string)
		  Me.id = id
		  Me.pole1 = pole1
		  me.pole2 = pole2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getID() As Integer
		  Return me.id
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getPole1() As String
		  Return me.pole1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getPole2() As String
		  Return me.pole2
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private id As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private pole1 As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private pole2 As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

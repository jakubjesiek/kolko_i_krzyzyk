#tag DesktopWindow
Begin DesktopWindow oknoLogowania
   Backdrop        =   0
   BackgroundColor =   &cFFFFFF
   Composite       =   False
   DefaultLocation =   2
   FullScreen      =   False
   HasBackgroundColor=   False
   HasCloseButton  =   True
   HasFullScreenButton=   False
   HasMaximizeButton=   False
   HasMinimizeButton=   True
   Height          =   315
   ImplicitInstance=   True
   MacProcID       =   0
   MaximumHeight   =   315
   MaximumWidth    =   400
   MenuBar         =   2104229887
   MenuBarVisible  =   False
   MinimumHeight   =   315
   MinimumWidth    =   400
   Resizeable      =   False
   Title           =   "Logowanie"
   Type            =   0
   Visible         =   False
   Width           =   400
   Begin DesktopTextField poleLogin
      AllowAutoDeactivate=   True
      AllowFocusRing  =   True
      AllowSpellChecking=   False
      AllowTabs       =   False
      BackgroundColor =   &cFFFFFF
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Format          =   ""
      HasBorder       =   True
      Height          =   22
      Hint            =   "login"
      Index           =   -2147483648
      Italic          =   False
      Left            =   100
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MaximumCharactersAllowed=   0
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextAlignment   =   0
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   148
      Transparent     =   False
      Underline       =   False
      ValidationMask  =   ""
      Visible         =   True
      Width           =   200
   End
   Begin DesktopTextField poleHaslo
      AllowAutoDeactivate=   True
      AllowFocusRing  =   True
      AllowSpellChecking=   False
      AllowTabs       =   False
      BackgroundColor =   &cFFFFFF
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Format          =   ""
      HasBorder       =   True
      Height          =   22
      Hint            =   "haslo"
      Index           =   -2147483648
      Italic          =   False
      Left            =   100
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MaximumCharactersAllowed=   0
      Password        =   True
      ReadOnly        =   False
      Scope           =   2
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextAlignment   =   0
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   182
      Transparent     =   False
      Underline       =   False
      ValidationMask  =   ""
      Visible         =   True
      Width           =   200
   End
   Begin DesktopButton przyciskZaloguj
      AllowAutoDeactivate=   True
      Bold            =   False
      Cancel          =   False
      Caption         =   "Zaloguj"
      Default         =   True
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   160
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MacButtonStyle  =   0
      Scope           =   2
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   267
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin URLConnection polaczenieURM
      AllowCertificateValidation=   False
      HTTPStatusCode  =   0
      Index           =   -2147483648
      LockedInPosition=   False
      Scope           =   2
      TabPanelIndex   =   0
   End
   Begin DesktopLabel linkRejestracja
      AllowAutoDeactivate=   True
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   100
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   2
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Załóż konto"
      TextAlignment   =   0
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   228
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   200
   End
   Begin DesktopLabel linkGenerowanieHasla
      AllowAutoDeactivate=   True
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   100
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   2
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Nie pamiętam hasła "
      TextAlignment   =   0
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   208
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   200
   End
   Begin DesktopCanvas canvasLogo
      AllowAutoDeactivate=   True
      AllowFocus      =   False
      AllowFocusRing  =   True
      AllowTabs       =   False
      Backdrop        =   152678399
      Enabled         =   True
      Height          =   109
      Index           =   -2147483648
      Left            =   100
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   2
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   20
      Transparent     =   True
      Visible         =   True
      Width           =   200
   End
End
#tag EndDesktopWindow

#tag WindowCode
	#tag Event
		Sub Opening()
		  ustawWyglad
		  HalpressID.inicjuj
		  If Not HalpressID.odczytajDaneZBazy(polaczenieURM) Then
		    pokazOknoLogowanie
		  End If
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub pokazOknoLogowanie()
		  Self.Visible = True
		  self.Show
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ustawWyglad()
		  //okno
		  oknoLogowania.HasBackgroundColor = True
		  oknoLogowania.BackgroundColor = kolorTlo
		  oknoLogowania.Title = napisTytulOkna
		  
		  // napisy
		  linkGenerowanieHasla.TextColor = kolorTekst
		  linkRejestracja.TextColor = kolorTekst
		  
		  linkGenerowanieHasla.Text = napisNiePamietamHasla
		  linkRejestracja.Text = napisZalozKonto
		  
		  poleHaslo.Hint = napisHintHaslo
		  poleLogin.Hint = napisHintLogin
		  
		  //przycisk
		  przyciskZaloguj.Caption = napisPrzyciskZaloguj
		  
		End Sub
	#tag EndMethod


	#tag Note, Name = README
		
		---------------------------------------
		
		Do app należy dodać stałe:
		 - idAplikacji as Number
		
		---------------------------------------
		
		Hexy kolorów w module do logowania:
		kolorTekst = DBDBDB
		kolorTekstCover = E1B03D
		kolorTlo = 3B3B3B
		
		-------------------------------------
		
		Jak korzystać z modułu logowanie?
		
		Domyślnie idAplikacji wynosi 256, jednak kiedy zaczniesz tworzyć własną, 
		należy podać Paulinie lub Dominikowi jej nazwę i otrzymasz własne idAplikacji.
		
		Aby móc się do niej zalogować  z poziomem uprawnień 0
		zarejestruj się na stronie: 
		www.halpress.team/rejestracja
		
		Aby zmieniać uprawnienia użytkowników aplikacji należy skorzystać z terminala:
		https://hid.xojocloud.net?cmd=terminal
	#tag EndNote


	#tag Constant, Name = napisHintHaslo, Type = String, Dynamic = True, Default = \"password", Scope = Private
		#Tag Instance, Platform = Any, Language = en, Definition  = \"password"
		#Tag Instance, Platform = Any, Language = pl, Definition  = \"has\xC5\x82o"
	#tag EndConstant

	#tag Constant, Name = napisHintLogin, Type = String, Dynamic = True, Default = \"e-mail", Scope = Private
		#Tag Instance, Platform = Any, Language = en, Definition  = \"e-mail"
		#Tag Instance, Platform = Any, Language = pl, Definition  = \"e-mail"
	#tag EndConstant

	#tag Constant, Name = napisNiePamietamHasla, Type = String, Dynamic = True, Default = \"Forgot password", Scope = Private
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Forgot password"
		#Tag Instance, Platform = Any, Language = pl, Definition  = \"Nie pami\xC4\x99tam has\xC5\x82a"
	#tag EndConstant

	#tag Constant, Name = napisPrzyciskZaloguj, Type = String, Dynamic = True, Default = \"Log in", Scope = Private
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Log in"
		#Tag Instance, Platform = Any, Language = pl, Definition  = \"Zaloguj"
	#tag EndConstant

	#tag Constant, Name = napisTytulOkna, Type = String, Dynamic = True, Default = \"Sign in", Scope = Private
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Sign in"
		#Tag Instance, Platform = Any, Language = pl, Definition  = \"Logowanie"
	#tag EndConstant

	#tag Constant, Name = napisZalozKonto, Type = String, Dynamic = True, Default = \"Register", Scope = Private
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Register"
		#Tag Instance, Platform = Any, Language = pl, Definition  = \"Zarejestruj"
	#tag EndConstant


#tag EndWindowCode

#tag Events przyciskZaloguj
	#tag Event
		Sub Pressed()
		  If poleLogin.Text.Length > 5 And poleHaslo.Text.Length > 5   Then
		    HalpressID.zalogujUzytkownika(polaczenieURM, poleLogin.Text, poleHaslo.Text)
		  Else
		    MessageBox(komunikatBledneDaneLogowania)
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events polaczenieURM
	#tag Event
		Sub ContentReceived(URL As String, HTTPStatus As Integer, content As String)
		  // Jeżeli dane logowania się zgadzają, to otwiera się okno aplikacji, 
		  // w przeciwnym wypadku pojawia się komunikat
		  
		  If HalpressID.rozpakujOdpowiedzURM(content) Then 
		    // #TODO Okno pojawiające się po logowaniu
		    oknoGlowne.Show
		    Self.Close
		  Else
		    pokazOknoLogowanie
		  End If
		End Sub
	#tag EndEvent
	#tag Event
		Sub Error(e As RuntimeException)
		  MessageBox(komunikatBleduPolaczenieURM + _
		  EndOfLine + EndOfLine + e.Message)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events linkRejestracja
	#tag Event
		Function MouseDown(x As Integer, y As Integer) As Boolean
		  // wejście w link
		  Const wwwZarejestruj As String = "https://hid.xojocloud.net/?cmd=register"
		  
		  oknoWWW.oknoWWW.LoadURL(wwwZarejestruj)
		End Function
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  // zamiana koloru napisu na domyślny
		  Me.TextColor = kolorTekst
		  me.Bold = false
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  // zamiana koloru napisu
		  Me.TextColor = kolorTekstCover
		  me.Bold = true
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events linkGenerowanieHasla
	#tag Event
		Function MouseDown(x As Integer, y As Integer) As Boolean
		  // wejście w link
		  Const wwwGenerujHaslo As String = "https://hid.xojocloud.net/?cmd=pin"
		  
		  oknoWWW.oknoWWW.LoadURL(wwwGenerujHaslo)
		End Function
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  // zamiana koloru napisu
		  Me.TextColor = kolorTekstCover
		  me.Bold = true
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  // zamiana koloru napisu na domyślny
		  Me.TextColor = kolorTekst
		  me.Bold = false
		End Sub
	#tag EndEvent
#tag EndEvents

#tag Class
Protected Class UzytkownikZalogowany
Inherits Uzytkownik
	#tag Method, Flags = &h0
		Sub Constructor(mail as String, token as String, poziomUprawnien as Integer)
		  Me.mailU = mail
		  Me.token = token
		  me.poziomUprawnien = poziomUprawnien
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getToken() As String
		  return token
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub setToken(token as String)
		  me.token = token
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private token As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

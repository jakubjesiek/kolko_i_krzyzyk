#tag Class
Protected Class UzytkownikDomyslny
Inherits Uzytkownik
	#tag Method, Flags = &h0
		Sub Constructor(mailU as String, hashHaslo as String, idU as Integer, poziomUprawnien as Integer)
		  Me.mailU = mailU
		  Me.hashHasloU = hashHaslo
		  Me.poziomUprawnien = poziomUprawnien
		  me.idU = idU
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getIdU() As Integer
		  Return idU
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub setIdU(idU as Integer)
		  If idU > 255 Then
		    Me.idU = idU
		  End If
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private idU As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

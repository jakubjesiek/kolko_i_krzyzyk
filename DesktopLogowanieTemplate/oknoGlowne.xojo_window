#tag DesktopWindow
Begin DesktopWindow oknoGlowne
   Backdrop        =   0
   BackgroundColor =   &cFFFFFF
   Composite       =   False
   DefaultLocation =   2
   FullScreen      =   False
   HasBackgroundColor=   False
   HasCloseButton  =   True
   HasFullScreenButton=   False
   HasMaximizeButton=   True
   HasMinimizeButton=   True
   Height          =   740
   ImplicitInstance=   True
   MacProcID       =   0
   MaximumHeight   =   740
   MaximumWidth    =   640
   MenuBar         =   932997119
   MenuBarVisible  =   False
   MinimumHeight   =   740
   MinimumWidth    =   640
   Resizeable      =   True
   Title           =   "Kółko i krzyżyk"
   Type            =   0
   Visible         =   True
   Width           =   640
   Begin DesktopBevelButton przycisk0
      Active          =   False
      AllowAutoDeactivate=   True
      AllowFocus      =   True
      AllowTabStop    =   True
      BackgroundColor =   &c00000000
      BevelStyle      =   0
      Bold            =   False
      ButtonStyle     =   0
      Caption         =   "Untitled"
      CaptionAlignment=   3
      CaptionDelta    =   0
      CaptionPosition =   1
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      HasBackgroundColor=   False
      Height          =   200
      Icon            =   0
      IconAlignment   =   0
      IconDeltaX      =   0
      IconDeltaY      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuStyle       =   0
      PanelIndex      =   0
      Scope           =   2
      TabIndex        =   0
      TabPanelIndex   =   0
      TextColor       =   &c00000000
      Tooltip         =   ""
      Top             =   20
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   200
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopBevelButton przycisk1
      Active          =   False
      AllowAutoDeactivate=   True
      AllowFocus      =   True
      AllowTabStop    =   True
      BackgroundColor =   &c00000000
      BevelStyle      =   0
      Bold            =   False
      ButtonStyle     =   0
      Caption         =   "Untitled"
      CaptionAlignment=   3
      CaptionDelta    =   0
      CaptionPosition =   1
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      HasBackgroundColor=   False
      Height          =   200
      Icon            =   0
      IconAlignment   =   0
      IconDeltaX      =   0
      IconDeltaY      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   220
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuStyle       =   0
      PanelIndex      =   0
      Scope           =   2
      TabIndex        =   1
      TabPanelIndex   =   0
      TextColor       =   &c00000000
      Tooltip         =   ""
      Top             =   20
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   200
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopBevelButton przycisk2
      Active          =   False
      AllowAutoDeactivate=   True
      AllowFocus      =   True
      AllowTabStop    =   True
      BackgroundColor =   &c00000000
      BevelStyle      =   0
      Bold            =   False
      ButtonStyle     =   0
      Caption         =   "Untitled"
      CaptionAlignment=   3
      CaptionDelta    =   0
      CaptionPosition =   1
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      HasBackgroundColor=   False
      Height          =   200
      Icon            =   0
      IconAlignment   =   0
      IconDeltaX      =   0
      IconDeltaY      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   420
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuStyle       =   0
      PanelIndex      =   0
      Scope           =   2
      TabIndex        =   2
      TabPanelIndex   =   0
      TextColor       =   &c00000000
      Tooltip         =   ""
      Top             =   20
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   200
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopBevelButton przycisk3
      Active          =   False
      AllowAutoDeactivate=   True
      AllowFocus      =   True
      AllowTabStop    =   True
      BackgroundColor =   &c00000000
      BevelStyle      =   0
      Bold            =   False
      ButtonStyle     =   0
      Caption         =   "Untitled"
      CaptionAlignment=   3
      CaptionDelta    =   0
      CaptionPosition =   1
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      HasBackgroundColor=   False
      Height          =   200
      Icon            =   0
      IconAlignment   =   0
      IconDeltaX      =   0
      IconDeltaY      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuStyle       =   0
      PanelIndex      =   0
      Scope           =   2
      TabIndex        =   3
      TabPanelIndex   =   0
      TextColor       =   &c00000000
      Tooltip         =   ""
      Top             =   220
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   200
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopBevelButton przycisk4
      Active          =   False
      AllowAutoDeactivate=   True
      AllowFocus      =   True
      AllowTabStop    =   True
      BackgroundColor =   &c00000000
      BevelStyle      =   0
      Bold            =   False
      ButtonStyle     =   0
      Caption         =   "Untitled"
      CaptionAlignment=   3
      CaptionDelta    =   0
      CaptionPosition =   1
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      HasBackgroundColor=   False
      Height          =   200
      Icon            =   0
      IconAlignment   =   0
      IconDeltaX      =   0
      IconDeltaY      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   220
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuStyle       =   0
      PanelIndex      =   0
      Scope           =   2
      TabIndex        =   4
      TabPanelIndex   =   0
      TextColor       =   &c00000000
      Tooltip         =   ""
      Top             =   220
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   200
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopBevelButton przycisk5
      Active          =   False
      AllowAutoDeactivate=   True
      AllowFocus      =   True
      AllowTabStop    =   True
      BackgroundColor =   &c00000000
      BevelStyle      =   0
      Bold            =   False
      ButtonStyle     =   0
      Caption         =   "Untitled"
      CaptionAlignment=   3
      CaptionDelta    =   0
      CaptionPosition =   1
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      HasBackgroundColor=   False
      Height          =   200
      Icon            =   0
      IconAlignment   =   0
      IconDeltaX      =   0
      IconDeltaY      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   420
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuStyle       =   0
      PanelIndex      =   0
      Scope           =   2
      TabIndex        =   5
      TabPanelIndex   =   0
      TextColor       =   &c00000000
      Tooltip         =   ""
      Top             =   220
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   200
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopBevelButton przycisk6
      Active          =   False
      AllowAutoDeactivate=   True
      AllowFocus      =   True
      AllowTabStop    =   True
      BackgroundColor =   &c00000000
      BevelStyle      =   0
      Bold            =   False
      ButtonStyle     =   0
      Caption         =   "Untitled"
      CaptionAlignment=   3
      CaptionDelta    =   0
      CaptionPosition =   1
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      HasBackgroundColor=   False
      Height          =   200
      Icon            =   0
      IconAlignment   =   0
      IconDeltaX      =   0
      IconDeltaY      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuStyle       =   0
      PanelIndex      =   0
      Scope           =   2
      TabIndex        =   6
      TabPanelIndex   =   0
      TextColor       =   &c00000000
      Tooltip         =   ""
      Top             =   420
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   200
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopBevelButton przycisk7
      Active          =   False
      AllowAutoDeactivate=   True
      AllowFocus      =   True
      AllowTabStop    =   True
      BackgroundColor =   &c00000000
      BevelStyle      =   0
      Bold            =   False
      ButtonStyle     =   0
      Caption         =   "Untitled"
      CaptionAlignment=   3
      CaptionDelta    =   0
      CaptionPosition =   1
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      HasBackgroundColor=   False
      Height          =   200
      Icon            =   0
      IconAlignment   =   0
      IconDeltaX      =   0
      IconDeltaY      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   220
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuStyle       =   0
      PanelIndex      =   0
      Scope           =   2
      TabIndex        =   7
      TabPanelIndex   =   0
      TextColor       =   &c00000000
      Tooltip         =   ""
      Top             =   420
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   200
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopBevelButton przycisk8
      Active          =   False
      AllowAutoDeactivate=   True
      AllowFocus      =   True
      AllowTabStop    =   True
      BackgroundColor =   &c00000000
      BevelStyle      =   0
      Bold            =   False
      ButtonStyle     =   0
      Caption         =   "Untitled"
      CaptionAlignment=   3
      CaptionDelta    =   0
      CaptionPosition =   1
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      HasBackgroundColor=   False
      Height          =   200
      Icon            =   0
      IconAlignment   =   0
      IconDeltaX      =   0
      IconDeltaY      =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   420
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuStyle       =   0
      PanelIndex      =   0
      Scope           =   2
      TabIndex        =   8
      TabPanelIndex   =   0
      TextColor       =   &c00000000
      Tooltip         =   ""
      Top             =   420
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   200
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopLabel napisWynikMeczu
      AllowAutoDeactivate=   True
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   46
      Index           =   -2147483648
      Italic          =   False
      Left            =   101
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   2
      Selectable      =   False
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0:0"
      TextAlignment   =   2
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   643
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   136
   End
   Begin DesktopButton przyciskZresetujGre
      AllowAutoDeactivate=   True
      Bold            =   False
      Cancel          =   False
      Caption         =   "Zresetuj grę"
      Default         =   True
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   30
      Index           =   -2147483648
      Italic          =   False
      Left            =   480
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MacButtonStyle  =   0
      Scope           =   2
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   690
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   140
   End
   Begin DesktopLabel napisKomputer
      AllowAutoDeactivate=   True
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   54
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   2
      Selectable      =   False
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "KOMPUTER"
      TextAlignment   =   0
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   655
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin DesktopLabel napisGracz
      AllowAutoDeactivate=   True
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   220
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   2
      Selectable      =   False
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "GRACZ"
      TextAlignment   =   0
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   655
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
End
#tag EndDesktopWindow

#tag WindowCode
	#tag Event
		Sub Opening()
		  ustawWartościPoczątkowe
		  Messagebox("Witamy Drogi Użytkowniku w grze Kółko i Krzyżyk :D"+EndOfLine+"W celu wybrania danego pola należy w nie kliknąć!")
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function czyGraczWygral() As Boolean
		  //Metoda sprawdza nam czy po danym ruchu gracz wygrał
		  for i as integer = 0 to 2
		    //sprawdzamy czy jest zgodność w rzędach
		    if przyciski(3*i).Icon=kolko and przyciski(3*i+1).Icon=kolko and przyciski(3*i+2).Icon=kolko then
		      return true
		    end
		    
		    //sprawdzamy czy jest zgodność w kolumnach
		    if przyciski(0+i).Icon=kolko and przyciski(3+i).Icon=kolko and przyciski(6+i).Icon=kolko then
		      return true
		    end
		    
		    //sprawdzamy czy jest zgodność w przekątnej
		    if przyciski(0).Icon=kolko and przyciski(4).Icon=kolko and przyciski(8).Icon=kolko then
		      return true
		    end
		    
		    if przyciski(2).Icon=kolko and przyciski(4).Icon=kolko and przyciski(6).Icon=kolko then
		      return true
		    end
		    
		  next
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function czyKomputerWygral() As Boolean
		  //Metoda sprawdza czy po danym ruchu komputer wygrał
		  for i as integer = 0 to 2
		    //sprawdzamy czy jest zgodność w rzędach
		    if przyciski(3*i).Icon=krzyzyk and przyciski(3*i+1).Icon=krzyzyk and przyciski(3*i+2).Icon=krzyzyk then
		      return true
		    end
		    
		    //sprawdzamy czy jest zgodność w kolumnach
		    if przyciski(0+i).Icon=krzyzyk and przyciski(3+i).Icon=krzyzyk and przyciski(6+i).Icon=krzyzyk then
		      return true
		    end
		    
		    //sprawdzamy czy jest zgodność w przekątnej
		    if przyciski(0).Icon=krzyzyk and przyciski(4).Icon=krzyzyk and przyciski(8).Icon=krzyzyk then
		      return true
		    end
		    
		    if przyciski(2).Icon=krzyzyk and przyciski(4).Icon=krzyzyk and przyciski(6).Icon=krzyzyk then
		      return true
		    end
		    
		  next
		  
		  return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function czyRemis() As Boolean
		  //Metoda sprawdza czy wszystkie pola są już zajęte
		  for i as Integer = 0 to przyciski.LastIndex
		    if przyciski(i).Icon = zdjeciePustegoPola then
		      return false
		    end
		  next 
		  
		  return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function gdziePostawić(pola() As DesktopBevelButton, ikonaKomputera As Picture, ikonaGracza As Picture) As Integer
		  var indeksZajmowanyDoObrony as Integer = -1 // jeżeli nieoptymalne dla gracza jest bronienie się to przyjmuje wartość -1, 
		  // w przeciwnym wypadku przechowuje indeks przycisku, który powinien wybrać komputer, żeby się obronić
		  var liczbaZajetychPol as Integer = 9 // przechowuje liczbę pól, które zostały już wybrane przez gracza lub komputer
		  var indeksyWolnychPol() as Integer // przechowuje indeksy pól, które nie zostały jeszcze wybrane
		  var liczbaLosowana as Integer
		  
		  // w pierwszej kolejności sprawdzamy, czy w jakiejkolwiek 3-elementowej linii (wiersz, kolumna, przekątna) 
		  // 2 z 3 pól mają ten sam symbol, a 3 pole jest puste
		  // jeżeli warunek zachodzi dla pól komputera to zwracamy indeks pustego pola
		  // jeżeli warunek zachodzi dla pól gracza to zapisujemy indeks do zmiennej indeksZajmowanyDoObrony
		  
		  // zmienna indeksZajmowanyDoObrony jest potrzebna do przechowywania indeksu pola, które musilibyśmy 
		  // wybrać, żeby się obronić, gdyby żadne z pól nie daje komputerowi wygranej, a należy się bronić
		  // wartość domyślna tej zmiennej wynosi -1 i oznacza, że nie zachodzi potrzeba bronienia się
		  
		  
		  for i as integer = 0 to 2
		    // sprawdzanie rzędów
		    if pola(3*i).Icon = zdjeciePustegoPola and pola(1+3*i).icon <> zdjeciePustegoPola and pola(1+3*i).icon = pola(2+3*i).icon then
		      if pola(1+3*i).icon = ikonaKomputera then
		        return 3*i
		      else
		        indeksZajmowanyDoObrony = 3*i
		      end if 
		    end if
		    if pola(1+3*i).icon = zdjeciePustegoPola and pola(3*i).icon <> zdjeciePustegoPola and pola(3*i).icon = pola(2+3*i).icon then
		      if pola(3*i).icon = ikonaKomputera then
		        return 1+3*i
		      else
		        indeksZajmowanyDoObrony = 1+3*i
		      end if 
		    end if
		    if pola(2+3*i).icon = zdjeciePustegoPola and pola(3*i).icon <> zdjeciePustegoPola and pola(3*i).icon = pola(1+3*i).icon then
		      if pola(3*i).icon = ikonaKomputera then
		        return 2+3*i
		      else
		        indeksZajmowanyDoObrony = 2+3*i
		      end if 
		    end if
		    
		    // sprawdzanie kolumn
		    if pola(0+i).icon = zdjeciePustegoPola and pola(3+i).icon <> zdjeciePustegoPola and pola(6+i).icon = pola(3+i).icon then
		      if pola(3+i).icon = ikonaKomputera then
		        return 0+i
		      else
		        indeksZajmowanyDoObrony = 0+i
		      end if 
		    end if
		    if pola(3+i).icon = zdjeciePustegoPola and pola(0+i).icon <> zdjeciePustegoPola and pola(0+i).icon = pola(6+i).icon then
		      if pola(0+i).icon = ikonaKomputera then
		        return 3+i
		      else
		        indeksZajmowanyDoObrony = 3+i
		      end if 
		    end if
		    if pola(6+i).icon = zdjeciePustegoPola and pola(0+i).icon <> zdjeciePustegoPola and pola(0+i).icon = pola(3+i).icon then
		      if pola(0+i).icon = ikonaKomputera then
		        return 6+i
		      else
		        indeksZajmowanyDoObrony = 6+i
		      end if 
		    end if
		  next
		  
		  // sprawdzanie po przekątnej
		  if pola(0).icon = zdjeciePustegoPola and pola(4).icon <> zdjeciePustegoPola and pola(4).icon = pola(8).icon then
		    if pola(4).icon = ikonaKomputera then
		      return 0
		    else
		      indeksZajmowanyDoObrony = 0
		    end if 
		  end if
		  if pola(4).icon = zdjeciePustegoPola and pola(0).icon <> zdjeciePustegoPola and pola(0).icon = pola(8).icon then
		    if pola(0).icon = ikonaKomputera then
		      return 4
		    else
		      indeksZajmowanyDoObrony = 4
		    end if 
		  end if
		  if pola(8).icon = zdjeciePustegoPola and pola(0).icon <> zdjeciePustegoPola and pola(0).icon = pola(4).icon then
		    if pola(0).icon = ikonaKomputera then
		      return 8
		    else
		      indeksZajmowanyDoObrony = 8
		    end if 
		  end if
		  if pola(2).icon = zdjeciePustegoPola and pola(4).icon <> zdjeciePustegoPola and pola(4).icon = pola(6).icon then
		    if pola(4).icon = ikonaKomputera then
		      return 2
		    else
		      indeksZajmowanyDoObrony = 2
		    end if 
		  end if
		  if pola(4).icon = zdjeciePustegoPola and pola(2).icon <> zdjeciePustegoPola and pola(2).icon = pola(6).icon then
		    if pola(2).icon = ikonaKomputera then
		      return 4
		    else
		      indeksZajmowanyDoObrony = 4
		    end if 
		  end if
		  if pola(6).icon = zdjeciePustegoPola and pola(2).icon <> zdjeciePustegoPola and pola(2).icon = pola(4).icon then
		    if pola(2).icon = ikonaKomputera then
		      return 6
		    else
		      indeksZajmowanyDoObrony = 6
		    end if 
		  end if
		  
		  // sprawdzamy czy komputer powinien się bronić
		  if indeksZajmowanyDoObrony <> -1 then
		    return indeksZajmowanyDoObrony
		  end if
		  
		  // obliczamy liczbę wybranych już wcześniej pól i dodajemy indeksy wolnych pol do tablicy
		  for indeks as Integer = 0 to pola.LastIndex
		    if pola(indeks).icon = zdjeciePustegoPola then
		      liczbaZajetychPol = liczbaZajetychPol - 1
		      indeksyWolnychPol.Add(indeks)
		    end if
		  next
		  
		  // algorytm działania
		  
		  if liczbaZajetychPol = 0 then 
		    liczbaLosowana = System.Random.InRange(0,3)
		    if liczbaLosowana = 0 then
		      Return 0
		    end if
		    if liczbaLosowana = 1 then
		      Return 2
		    end if
		    if liczbaLosowana = 3 then
		      Return 6
		    end if
		    Return 8
		  end if
		  
		  if liczbaZajetychPol = 1 then
		    if pola(4).icon=zdjeciePustegoPola then
		      return 4
		    else 
		      liczbaLosowana = System.Random.InRange(0,3)
		      if liczbaLosowana = 0 then Return 0
		      if liczbaLosowana = 1 then Return 2
		      if liczbaLosowana = 3 then Return 6
		      Return 8
		    end if
		  end if
		  
		  if liczbaZajetychPol = 2 then
		    if pola(4).icon = ikonaGracza then
		      if pola(0).icon = ikonaKomputera then return 8
		      if pola(2).icon = ikonaKomputera then return 6
		      if pola(6).icon = ikonaKomputera then return 2
		      return 0
		    else
		      if pola(0).icon = ikonaKomputera then 
		        if pola(2).icon = ikonaGracza or pola(2).icon = ikonaGracza then return 6
		        if pola(6).icon = ikonaGracza or pola(3).icon = ikonaGracza then return 2
		        liczbaLosowana = System.Random.InRange(0,1)
		        if liczbaLosowana = 0 then
		          return 2
		        else
		          return 6
		        end if
		      end if
		      if pola(8).icon = ikonaKomputera then 
		        if pola(2).icon = ikonaGracza or pola(5).icon = ikonaGracza then return 6
		        if pola(6).icon = ikonaGracza or pola(7).icon = ikonaGracza then return 2
		        liczbaLosowana = System.Random.InRange(0,1)
		        if liczbaLosowana = 0 then
		          return 2
		        else
		          return 6
		        end if
		      end if
		      if pola(2).icon = ikonaKomputera then
		        if pola(0).icon = ikonaGracza or pola(1).icon = ikonaGracza then return 8
		        if pola(8).icon = ikonaGracza or pola(5).icon = ikonaGracza then return 0
		        liczbaLosowana = System.Random.InRange(0,1)
		        if liczbaLosowana = 0 then
		          return 0
		        else
		          return 8
		        end if
		      end if 
		      if pola(6).icon = ikonaKomputera then
		        if pola(0).icon = ikonaGracza or pola(3).icon = ikonaGracza then return 8
		        if pola(8).icon = ikonaGracza or pola(7).icon = ikonaGracza then return 0
		        liczbaLosowana = System.Random.InRange(0,1)
		        if liczbaLosowana = 0 then
		          return 0
		        else
		          return 8
		        end if
		      end if 
		    end if
		  end if 
		  
		  if liczbaZajetychPol = 3 then
		    if (pola(0).icon=ikonaGracza and pola(8).icon=ikonaGracza) or (pola(2).icon=ikonaGracza and pola(6).icon=ikonaGracza) then 
		      liczbaLosowana = System.Random.InRange(0,3)
		      if liczbaLosowana = 0 then Return 1
		      if liczbaLosowana = 1 then Return 3
		      if liczbaLosowana = 3 then Return 5
		      Return 7
		    end if
		  end if
		  
		  if liczbaZajetychPol = 4 then
		    if pola(4).icon=zdjeciePustegoPola then return 4
		    liczbaLosowana = System.Random.InRange(0,3)
		    if liczbaLosowana = 0 then
		      if pola(0).icon = zdjeciePustegoPola then return 0
		      if pola(2).icon = zdjeciePustegoPola then return 2
		      if pola(6).icon = zdjeciePustegoPola then return 6
		      if pola(8).icon = zdjeciePustegoPola then return 8
		    end if
		    if liczbaLosowana = 1 then
		      if pola(2).icon = zdjeciePustegoPola then return 2
		      if pola(6).icon = zdjeciePustegoPola then return 6
		      if pola(8).icon = zdjeciePustegoPola then return 8
		      if pola(0).icon = zdjeciePustegoPola then return 0
		    end if
		    if liczbaLosowana = 2 then
		      if pola(6).icon = zdjeciePustegoPola then return 6
		      if pola(8).icon = zdjeciePustegoPola then return 8
		      if pola(0).icon = zdjeciePustegoPola then return 0
		      if pola(2).icon = zdjeciePustegoPola then return 2
		    end if
		    if liczbaLosowana = 3 then
		      if pola(8).icon = zdjeciePustegoPola then return 8
		      if pola(0).icon = zdjeciePustegoPola then return 0
		      if pola(2).icon = zdjeciePustegoPola then return 2
		      if pola(6).icon = zdjeciePustegoPola then return 6
		    end if
		  end if 
		  
		  // losowanie jednego z wolnych pól, ponieważ każda z opcji przynosi tyle samo pożytku
		  
		  
		  liczbaLosowana = System.Random.InRange(0,8-liczbaZajetychPol)
		  return indeksyWolnychPol(liczbaLosowana)
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ustawWartościPoczątkowe()
		  //Metoda tworzy tablice przycisków, wcześniej usuwając stare (jeśli były)
		  przyciski.RemoveAll
		  przyciski.add(przycisk0)
		  przyciski.add(przycisk1)
		  przyciski.add(przycisk2)
		  przyciski.add(przycisk3)
		  przyciski.add(przycisk4)
		  przyciski.add(przycisk5)
		  przyciski.add(przycisk6)
		  przyciski.add(przycisk7)
		  przyciski.add(przycisk8)
		  
		  //Dodawanie ikony i napisu do przycisku
		  for i as Integer = 0 to przyciski.LastIndex
		    przyciski(i).Caption = ""
		    przyciski(i).Icon = zdjeciePustegoPola
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub zresetujWynik()
		  //Metoda resetuje wyniki komputera i gracza
		  wynikKomputer=0
		  wynikGracz=0
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		przyciski() As DesktopBevelButton
	#tag EndProperty

	#tag Property, Flags = &h21
		Private wynikGracz As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private wynikKomputer As Integer = 0
	#tag EndProperty


#tag EndWindowCode

#tag Events przycisk0
	#tag Event
		Sub Pressed()
		  //Sprawdzenie czy wybrany przycisk jest już zajęty
		  if przyciski(0).Icon = krzyzyk or przyciski(0).Icon = kolko then return
		  
		  //Jeśli przycisk nie jest zajęty to stawiamy znak gracza, czyli kółko
		  me.Caption = ""
		  przyciski(0).Icon = kolko
		  //Sprawdzamy czy to był ostatni ruch gracza i czy jest remis
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  else
		    //Jeśli nie ma remisu to sprawdzamy czy gracz wygrał
		    if czyGraczWygral() then
		      MessageBox("Gracz wygrał")
		      wynikGracz = wynikGracz + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    else
		      if czyRemis() then
		        MessageBox("REMIS")
		        wynikGracz = wynikGracz + 1
		        wynikKomputer = wynikKomputer + 1
		        napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		        ustawWartościPoczątkowe
		      else
		        //Jeśli nie ma remisu i gracz nie wygrał, swój ruch ma komputer
		        var odpowiedz as Integer = gdziePostawić(przyciski, krzyzyk, kolko)
		        przyciski(odpowiedz).Icon = krzyzyk
		      end
		    end
		    //Sprawdzamy czy po ruchu komputer wygrał
		    if czyKomputerWygral() then
		      MessageBox("Komputer wygrał")
		      wynikKomputer = wynikKomputer + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    end
		  end
		  //Jeśli komputer nie wygrał to sprawdzamy czy jest remis
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events przycisk1
	#tag Event
		Sub Pressed()
		  if przyciski(1).Icon = krzyzyk or przyciski(1).Icon = kolko then return
		  
		  
		  me.Caption = ""
		  przyciski(1).Icon = kolko
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  else
		    if czyGraczWygral() then
		      MessageBox("Gracz wygrał")
		      wynikGracz = wynikGracz + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    else
		      if czyRemis() then
		        MessageBox("REMIS")
		        wynikGracz = wynikGracz + 1
		        wynikKomputer = wynikKomputer + 1
		        napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		        ustawWartościPoczątkowe
		      else
		        var odpowiedz as Integer = gdziePostawić(przyciski, krzyzyk, kolko)
		        przyciski(odpowiedz).Icon = krzyzyk
		      end
		    end
		    if czyKomputerWygral() then
		      MessageBox("Komputer wygrał")
		      wynikKomputer = wynikKomputer + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    end
		  end
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events przycisk2
	#tag Event
		Sub Pressed()
		  if przyciski(2).Icon = krzyzyk or przyciski(2).Icon = kolko then return
		  
		  
		  me.Caption = ""
		  przyciski(2).Icon = kolko
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  else
		    if czyGraczWygral() then
		      MessageBox("Gracz wygrał")
		      wynikGracz = wynikGracz + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    else
		      if czyRemis() then
		        MessageBox("REMIS")
		        wynikGracz = wynikGracz + 1
		        wynikKomputer = wynikKomputer + 1
		        napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		        ustawWartościPoczątkowe
		      else
		        var odpowiedz as Integer = gdziePostawić(przyciski, krzyzyk, kolko)
		        przyciski(odpowiedz).Icon = krzyzyk
		      end
		    end
		    if czyKomputerWygral() then
		      MessageBox("Komputer wygrał")
		      wynikKomputer = wynikKomputer + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    end
		  end
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events przycisk3
	#tag Event
		Sub Pressed()
		  if przyciski(3).Icon = krzyzyk or przyciski(3).Icon = kolko then return
		  
		  
		  me.Caption = ""
		  przyciski(3).Icon = kolko
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  else
		    if czyGraczWygral() then
		      MessageBox("Gracz wygrał")
		      wynikGracz = wynikGracz + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    else
		      if czyRemis() then
		        MessageBox("REMIS")
		        wynikGracz = wynikGracz + 1
		        wynikKomputer = wynikKomputer + 1
		        napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		        ustawWartościPoczątkowe
		      else
		        var odpowiedz as Integer = gdziePostawić(przyciski, krzyzyk, kolko)
		        przyciski(odpowiedz).Icon = krzyzyk
		      end
		    end
		    if czyKomputerWygral() then
		      MessageBox("Komputer wygrał")
		      wynikKomputer = wynikKomputer + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    end
		  end
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events przycisk4
	#tag Event
		Sub Pressed()
		  if przyciski(4).Icon = krzyzyk or przyciski(4).Icon = kolko then return
		  
		  
		  me.Caption = ""
		  przyciski(4).Icon = kolko
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  else
		    if czyGraczWygral() then
		      MessageBox("Gracz wygrał")
		      wynikGracz = wynikGracz + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    else
		      if czyRemis() then
		        MessageBox("REMIS")
		        wynikGracz = wynikGracz + 1
		        wynikKomputer = wynikKomputer + 1
		        napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		        ustawWartościPoczątkowe
		      else
		        var odpowiedz as Integer = gdziePostawić(przyciski, krzyzyk, kolko)
		        przyciski(odpowiedz).Icon = krzyzyk
		      end
		    end
		    if czyKomputerWygral() then
		      MessageBox("Komputer wygrał")
		      wynikKomputer = wynikKomputer + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    end
		  end
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events przycisk5
	#tag Event
		Sub Pressed()
		  if przyciski(5).Icon = krzyzyk or przyciski(5).Icon = kolko then return
		  
		  
		  me.Caption = ""
		  przyciski(5).Icon = kolko
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  else
		    if czyGraczWygral() then
		      MessageBox("Gracz wygrał")
		      wynikGracz = wynikGracz + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    else
		      if czyRemis() then
		        MessageBox("REMIS")
		        wynikGracz = wynikGracz + 1
		        wynikKomputer = wynikKomputer + 1
		        napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		        ustawWartościPoczątkowe
		      else
		        var odpowiedz as Integer = gdziePostawić(przyciski, krzyzyk, kolko)
		        przyciski(odpowiedz).Icon = krzyzyk
		      end
		    end
		    if czyKomputerWygral() then
		      MessageBox("Komputer wygrał")
		      wynikKomputer = wynikKomputer + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    end
		  end
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events przycisk6
	#tag Event
		Sub Pressed()
		  if przyciski(6).Icon = krzyzyk or przyciski(6).Icon = kolko then return
		  
		  
		  me.Caption = ""
		  przyciski(6).Icon = kolko
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  else
		    if czyGraczWygral() then
		      MessageBox("Gracz wygrał")
		      wynikGracz = wynikGracz + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    else
		      if czyRemis() then
		        MessageBox("REMIS")
		        wynikGracz = wynikGracz + 1
		        wynikKomputer = wynikKomputer + 1
		        napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		        ustawWartościPoczątkowe
		      else
		        var odpowiedz as Integer = gdziePostawić(przyciski, krzyzyk, kolko)
		        przyciski(odpowiedz).Icon = krzyzyk
		      end
		    end
		    if czyKomputerWygral() then
		      MessageBox("Komputer wygrał")
		      wynikKomputer = wynikKomputer + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    end
		  end
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events przycisk7
	#tag Event
		Sub Pressed()
		  if przyciski(7).Icon = krzyzyk or przyciski(7).Icon = kolko then return
		  
		  
		  me.Caption = ""
		  przyciski(7).Icon = kolko
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  else
		    if czyGraczWygral() then
		      MessageBox("Gracz wygrał")
		      wynikGracz = wynikGracz + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    else
		      if czyRemis() then
		        MessageBox("REMIS")
		        wynikGracz = wynikGracz + 1
		        wynikKomputer = wynikKomputer + 1
		        napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		        ustawWartościPoczątkowe
		      else
		        var odpowiedz as Integer = gdziePostawić(przyciski, krzyzyk, kolko)
		        przyciski(odpowiedz).Icon = krzyzyk
		      end
		    end
		    if czyKomputerWygral() then
		      MessageBox("Komputer wygrał")
		      wynikKomputer = wynikKomputer + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    end
		  end
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events przycisk8
	#tag Event
		Sub Pressed()
		  if przyciski(8).Icon = krzyzyk or przyciski(8).Icon = kolko then return
		  
		  
		  me.Caption = ""
		  przyciski(8).Icon = kolko
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  else
		    if czyGraczWygral() then
		      MessageBox("Gracz wygrał")
		      wynikGracz = wynikGracz + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    else
		      if czyRemis() then
		        MessageBox("REMIS")
		        wynikGracz = wynikGracz + 1
		        wynikKomputer = wynikKomputer + 1
		        napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		        ustawWartościPoczątkowe
		      else
		        var odpowiedz as Integer = gdziePostawić(przyciski, krzyzyk, kolko)
		        przyciski(odpowiedz).Icon = krzyzyk
		      end
		    end
		    if czyKomputerWygral() then
		      MessageBox("Komputer wygrał")
		      wynikKomputer = wynikKomputer + 1
		      napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		      ustawWartościPoczątkowe
		    end
		  end
		  if czyRemis() then
		    MessageBox("REMIS")
		    wynikGracz = wynikGracz + 1
		    wynikKomputer = wynikKomputer + 1
		    napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		    ustawWartościPoczątkowe
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events przyciskZresetujGre
	#tag Event
		Sub Pressed()
		  //Metoda resetuje wyniki, przyciski oraz napisy
		  ustawWartościPoczątkowe
		  zresetujWynik
		  napisWynikMeczu.Text=wynikKomputer.ToText + ":" + wynikGracz.ToText
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		InitialValue=""
		Type="String"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		InitialValue=""
		Type="String"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		InitialValue=""
		Type="String"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="600"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="400"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimumWidth"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimumHeight"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximumWidth"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximumHeight"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Type"
		Visible=true
		Group="Frame"
		InitialValue="0"
		Type="Types"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Frame"
		InitialValue="Untitled"
		Type="String"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasCloseButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasMaximizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasMinimizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasFullScreenButton"
		Visible=true
		Group="Frame"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=false
		Group="OS X (Carbon)"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=false
		Group="OS X (Carbon)"
		InitialValue="0"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=false
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="DefaultLocation"
		Visible=true
		Group="Behavior"
		InitialValue="2"
		Type="Locations"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Windows Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackgroundColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackgroundColor"
		Visible=true
		Group="Background"
		InitialValue="&cFFFFFF"
		Type="ColorGroup"
		EditorType="ColorGroup"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		InitialValue=""
		Type="Picture"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Menus"
		InitialValue=""
		Type="DesktopMenuBar"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Deprecated"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
#tag EndViewBehavior

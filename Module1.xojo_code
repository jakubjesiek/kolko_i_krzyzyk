#tag Module
Protected Module Module1
	#tag Method, Flags = &h0
		Function gdziePostawić(pola() As pole) As Integer
		  var indeksZajmowanyDoObrony as Integer = -1
		  var liczbaZajetychPol as Integer = 9 // przechowuje liczbę pól, które zostały już wybrane przez gracza lub komputer
		  var indeksyWolnychPol() as Integer 
		  var liczbaLosowana as Integer
		  
		  // w pierwszej kolejności sprawdzamy, czy w jakiejkolwiek 3-elementowej linii (wiersz, kolumna, przekątna) 
		  // 2 z 3 pól mają ten sam symbol, a 3 pole jest puste
		  // jeżeli warunek zachodzi dla pól komputera to zwracamy indeks pustego pola
		  // jeżeli warunek zachodzi dla pól gracza to zapisujemy indeks do zmiennej indeksZajmowanyDoObrony
		  
		  // zmienna indeksZajmowanyDoObrony jest potrzebna do przechowywania indeksu pola, które musilibyśmy 
		  // wybrać, żeby się obronić, gdyby żadne z pól nie daje komputerowi wygranej, a należy się bronić
		  // wartość domyślna tej zmiennej wynosi -1 i oznacza, że nie zachodzi potrzeba bronienia się
		  
		  
		  for i as integer = 0 to 2
		    // sprawdzanie rzędów
		    if pola(3*i).symbol = "" and pola(1+3*i).symbol <> "" and pola(1+3*i).symbol = pola(2+3*i).symbol then
		      if pola(1+3*i).symbol = "komputer" then
		        return 3*i
		      else
		        indeksZajmowanyDoObrony = 3*i
		      end if 
		    end if
		    if pola(1+3*i).symbol = "" and pola(3*i).symbol <> "" and pola(3*i).symbol = pola(2+3*i).symbol then
		      if pola(3*i).symbol = "komputer" then
		        return 1+3*i
		      else
		        indeksZajmowanyDoObrony = 1+3*i
		      end if 
		    end if
		    if pola(2+3*i).symbol = "" and pola(3*i).symbol <> "" and pola(3*i).symbol = pola(1+3*i).symbol then
		      if pola(3*i).symbol = "komputer" then
		        return 2+3*i
		      else
		        indeksZajmowanyDoObrony = 2+3*i
		      end if 
		    end if
		    
		    // sprawdzanie kolumn
		    if pola(0+i).symbol = "" and pola(3+i).symbol <> "" and pola(6+i).symbol = pola(3+i).symbol then
		      if pola(3+i).symbol = "komputer" then
		        return 0+i
		      else
		        indeksZajmowanyDoObrony = 0+i
		      end if 
		    end if
		    if pola(3+i).symbol = "" and pola(0+i).symbol <> "" and pola(0+i).symbol = pola(6+i).symbol then
		      if pola(0+i).symbol = "komputer" then
		        return 3+i
		      else
		        indeksZajmowanyDoObrony = 3+i
		      end if 
		    end if
		    if pola(6+i).symbol = "" and pola(0+i).symbol <> "" and pola(0+i).symbol = pola(3+i).symbol then
		      if pola(0+i).symbol = "komputer" then
		        return 6+i
		      else
		        indeksZajmowanyDoObrony = 6+i
		      end if 
		    end if
		  next
		  
		  // sprawdzanie po przekątnej
		  if pola(0).symbol = "" and pola(4).symbol <> "" and pola(4).symbol = pola(8).symbol then
		    if pola(4).symbol = "komputer" then
		      return 0
		    else
		      indeksZajmowanyDoObrony = 0
		    end if 
		  end if
		  if pola(4).symbol = "" and pola(0).symbol <> "" and pola(0).symbol = pola(8).symbol then
		    if pola(0).symbol = "komputer" then
		      return 4
		    else
		      indeksZajmowanyDoObrony = 4
		    end if 
		  end if
		  if pola(8).symbol = "" and pola(0).symbol <> "" and pola(0).symbol = pola(4).symbol then
		    if pola(0).symbol = "komputer" then
		      return 8
		    else
		      indeksZajmowanyDoObrony = 8
		    end if 
		  end if
		  if pola(2).symbol = "" and pola(4).symbol <> "" and pola(4).symbol = pola(6).symbol then
		    if pola(4).symbol = "komputer" then
		      return 2
		    else
		      indeksZajmowanyDoObrony = 2
		    end if 
		  end if
		  if pola(4).symbol = "" and pola(2).symbol <> "" and pola(2).symbol = pola(6).symbol then
		    if pola(2).symbol = "komputer" then
		      return 4
		    else
		      indeksZajmowanyDoObrony = 4
		    end if 
		  end if
		  if pola(6).symbol = "" and pola(2).symbol <> "" and pola(2).symbol = pola(4).symbol then
		    if pola(2).symbol = "komputer" then
		      return 6
		    else
		      indeksZajmowanyDoObrony = 6
		    end if 
		  end if
		  
		  // sprawdzamy czy komputer powinien się bronić
		  if indeksZajmowanyDoObrony <> -1 then
		    return indeksZajmowanyDoObrony
		  end if
		  
		  // obliczamy liczbę wybranych już wcześniej pól i dodajemy indeksy wolnych pol do tablicy
		  for indeks as Integer = 0 to pola.LastIndex
		    if pola(indeks).symbol = "" then
		      liczbaZajetychPol = liczbaZajetychPol - 1
		      indeksyWolnychPol.Add(indeks)
		    end if
		  next
		  
		  // algorytm działania
		  
		  if liczbaZajetychPol = 0 then Return 0
		  
		  if liczbaZajetychPol = 1 then
		    if pola(4).symbol="" then
		      return 4
		    else 
		      return 0
		    end if
		  end if
		  
		  if liczbaZajetychPol = 2 then
		    if pola(4).symbol = "gracz" then
		      return 3
		    else
		      return 8
		    end if
		  end if
		  
		  if liczbaZajetychPol = 3 then
		    if (pola(0).symbol="gracz" and pola(8).symbol="gracz") or (pola(2).symbol="gracz" and pola(6).symbol="gracz") then return 3
		  end if
		  
		  // losowanie jednego z wolnych pól, ponieważ każda z opcji przynosi tyle samo pożytku
		  
		  liczbaLosowana = System.Random.InRange(0,8-liczbaZajetychPol)
		  return indeksyWolnychPol(liczbaLosowana)
		  
		  
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
